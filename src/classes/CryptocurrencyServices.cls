public with sharing class CryptocurrencyServices {

    public static void getBulkPrices(List<Cryptocurrency__c> cryptocurrenciesWithoutPrice) {
        for (Cryptocurrency__c cryptocurrency : cryptocurrenciesWithoutPrice) {
            getPriceFuture(cryptocurrency.Id, cryptocurrency.Name);
        }
    }

    @Future(Callout=true)
    public static void getPriceFuture(Id cryptoId, String cryptoName) {
        Decimal cryptoPrice = getCryptoDetails(cryptoName);
        String cryptoNewName = getCryptoNames(cryptoName);
        String cryptoIconURL = getIconURL(cryptoName);
        update new Cryptocurrency__c(Id = cryptoId, Price_In_USD__c = cryptoPrice, Abbrev__c = cryptoNewName, IconsURL__c = cryptoIconURL);
    }

    public static Decimal getCryptoDetails(String cryptoName) {
        Decimal cryptoPriceInUSD;
        String requestEndPoint = ('callout:Coin_API/' + cryptoName.toUpperCase() + '/USD');
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(requestEndPoint);
        request.setMethod('GET');
        request.setHeader('X-CoinAPI-Key', '{!$Credential.Password}');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            cryptoPriceInUSD = Decimal.valueOf((Double) results.get('rate')).setScale(2);
            System.debug(cryptoPriceInUSD);
        } else {
            System.debug(response.getBody());
        }
        return cryptoPriceInUSD;
    }

    public static String getCryptoNames(String cryptoName) {
        String name;
        String requestEndPoint = ('callout:CryptoNames/' + cryptoName);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(requestEndPoint);
        request.setMethod('GET');
        request.setHeader('X-CoinAPI-Key', '{!$Credential.Password}');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            List<Object> result = (List<Object>) JSON.deserializeUntyped(response.getBody());
            for (Object obj : result) {
                Map<String, Object> mapResult = (Map<String, Object>) obj;
                name = (String) mapResult.get('name');
                System.debug(name);
            }
        } else {
            System.debug(response.getBody());
        }
        return name;
    }

    public static String getIconURL(String cryptoName) {
        String asset_id;
        String cryptoId;
        String finalURL;
        String requestEndPoint = ('callout:Icons/');

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(requestEndPoint);
        request.setMethod('GET');
        request.setHeader('X-CoinAPI-Key', '{!$Credential.Password}');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            List<Object> result = (List<Object>) JSON.deserializeUntyped(response.getBody());
            for (Object obj : result) {
                Map<String, Object> mapResult = (Map<String, Object>) obj;
                asset_id = (String) mapResult.get('asset_id');
                if (asset_id == cryptoName) {
                    cryptoId = asset_id;
                    finalURL = (String) mapResult.get('url');
                }
            }
        } else {
            System.debug(response.getBody());
        }
        System.debug(finalURL);
        return finalURL;
    }

    @AuraEnabled
    public static List<Cryptocurrency__c> getDataCryptocurrencies(String field, String sortOrder) {
        String query = 'SELECT Id, Name, Price_In_USD__c, Abbrev__c, IconsURL__c, Short_Description__c FROM Cryptocurrency__c';
        query += ' ORDER BY ' + field + ' ' + sortOrder;
        return Database.query(query);
    }

    @AuraEnabled
    public static Cryptocurrency__c saveCryptocurrency(Cryptocurrency__c cryptocurrency) {
        upsert cryptocurrency;
        return cryptocurrency;
    }

    @AuraEnabled
    public static String DownloadPDFData() {
        System.PageReference pageRef = new System.PageReference('/apex/CryptocurrencyTablePDF');
        Blob pdfBody;
        if (Test.isRunningTest()) {
            pdfBody = Blob.valueOf('I am String to be converted in base64 encoding!');
        } else {
            pdfBody = pageRef.getContentAsPDF();
        }

        pageRef.getHeaders().put('content-disposition', 'attachment; filename=CryptocurrencyExchangeRatesReport.pdf');
        System.debug(EncodingUtil.base64Encode(pdfBody));
        return EncodingUtil.base64Encode(pdfBody);
    }

    public static void getTotalAmountOfCryptocurrencies(Set<Id> AccountIds) {
        List<AggregateResult> results = [
                SELECT Account__c, COUNT(Id) countId
                FROM Cryptocurrency_Account__c
                WHERE Account__c IN:AccountIds
                GROUP BY Account__c
        ];

        List<Account> accounts = new List<Account>();
        for (AggregateResult result : results) {
            Account account = new Account (Id = (Id) result.get('Account__c'),
                    Total_Cryptocurrencies__c = (Integer) result.get('countId'));

            accounts.add(account);
        }
        update accounts;
    }

    public static void populateFieldWithRandomNumber() {
        List<Cryptocurrency__c> cryptocurrencies = new List<Cryptocurrency__c>();
        for (Cryptocurrency__c cryptocurrency : (List<Cryptocurrency__c>) Trigger.new) {
            cryptocurrencies.add(new Cryptocurrency__c(Id = cryptocurrency.Id, random_number__c = getRandomInteger(), random_number_2__c = getRandomInteger()));
        }
        update cryptocurrencies;
    }

    public static Integer getRandomInteger() {
        Integer result = (Integer) (Math.random() * 1000);
        return result;
    }
}