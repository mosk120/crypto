public class ScheduledProcessJob implements CryptocurrencySchedulable.IScheduleDispatched {
    public static final String CRON_EXPRESSION = '0 0 22 * * ?';
    public static final String STATE_COMPLETED = 'COMPLETED';
    public static final String STATE_ERROR = 'ERROR';
    public static final String STATE_DELETED = 'DELETED';

    public void executeTask(SchedulableContext sc) {
        try {
            Database.executeBatch(new CryptocurrencyBatch(), 1);
        } catch (Exception exc) {
            System.abortJob(sc.getTriggerId());
            return;
        }
        ScheduledProcessJob.startScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
    }

    public static String startScheduler(String jobName) {
        List<CronTrigger> jobs = [
                SELECT Id, CronJobDetail.Name, State, NextFireTime
                FROM CronTrigger
                WHERE CronJobDetail.Name = :jobName
        ];
        if (jobs.size() > 0
                && !(jobs[0].State == STATE_COMPLETED)
                && !(jobs[0].State == STATE_ERROR)
                && !(jobs[0].State == STATE_DELETED)
                ) {
            return jobs[0].Id;
        }
        if (jobs.size() > 0) {
            for (CronTrigger job : jobs) {
                System.abortJob(job.Id);
            }
        }
        String jobId = System.schedule(jobName, CRON_EXPRESSION, new CryptocurrencySchedulable());
        return jobId;
    }

    public static Boolean stopScheduler(String jobName) {
        List<CronTrigger> jobs = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = :jobName
        ];
        try {
            if (jobs.size() > 0) {
                for (CronTrigger job : jobs) {
                    System.abortJob(job.Id);
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static Boolean isSchedulerRunning(String jobName) {
        List<CronTrigger> jobs = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = :jobName
        ];
        if (jobs.size() > 0) {
            return true;
        }
        return false;
    }
}