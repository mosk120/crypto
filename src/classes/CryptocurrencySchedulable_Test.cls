@IsTest
private class CryptocurrencySchedulable_Test {

    final static String CRON_EXPRESSION = '0 0 23 * * ?';

    @IsTest
    static void test_execute() {
        Test.startTest();
        CryptocurrencySchedulable myClass = new CryptocurrencySchedulable();
        System.schedule('Test Schedule', CRON_EXPRESSION, myClass);
        Test.stopTest();
    }
}