public class TotalCryptocurrenciesTriggerHandler extends TriggerHandler {

    public override void afterInsert() {
        Set<Id> AccountIds = new Set<Id>();
        Utils.getValuesByFieldName(AccountIds, Trigger.new, 'Account__c');
        CryptocurrencyServices.getTotalAmountOfCryptocurrencies(AccountIds);
    }

    public override void afterDelete() {
        Set<Id> AccountIds = new Set<Id>();
        Utils.getValuesByFieldName(AccountIds, Trigger.old, 'Account__c');
        CryptocurrencyServices.getTotalAmountOfCryptocurrencies(AccountIds);
    }
}