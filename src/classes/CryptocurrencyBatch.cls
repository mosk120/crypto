public class CryptocurrencyBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {

    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Name, Abbrev__c, Price_In_USD__c, IconsURL__c FROM Cryptocurrency__c';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Cryptocurrency__c> scope) {
        for (Cryptocurrency__c cryptocurrency : scope) {
            Decimal price = CryptocurrencyServices.getCryptoDetails(cryptocurrency.Name);
            String abbrev = CryptocurrencyServices.getCryptoNames(cryptocurrency.Name);
            String cryptoURL = CryptocurrencyServices.getIconURL(cryptocurrency.Name);
            if (cryptocurrency.Name != null) {
                cryptocurrency.Price_In_USD__c = price;
                cryptocurrency.Abbrev__c = abbrev;
                cryptocurrency.IconsURL__c = cryptoURL;
            }
        }
        update scope;
    }

    public void finish(Database.BatchableContext BC) {

    }

}