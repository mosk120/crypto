public class CryptocurrencyTablePDFController {
    List<Cryptocurrency__c> cryptocurrencies;

    public List<Cryptocurrency__c> getCurrencies() {
            cryptocurrencies = [SELECT Id, Name, Price_In_USD__c, Abbrev__c, IconsURL__c
            FROM Cryptocurrency__c
            ORDER BY Price_In_USD__c
            DESC];
            return cryptocurrencies;
    }
}