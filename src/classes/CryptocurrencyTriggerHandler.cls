public class CryptocurrencyTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        Utils.fieldIsEmptyError(Trigger.new, 'Short_Description__c', 'Populate the field');
    }

    public override void afterInsert() {
        List<Cryptocurrency__c> cryptocurrenciesWithoutPrice = new List<Cryptocurrency__c>();
        for (Cryptocurrency__c cryptocurrency : (List<Cryptocurrency__c>) Trigger.new) {
            if (cryptocurrency.Price_In_USD__c == null) {
                cryptocurrenciesWithoutPrice.add(cryptocurrency);
            }
        }
        CryptocurrencyServices.getBulkPrices(cryptocurrenciesWithoutPrice);
        CryptocurrencyServices.populateFieldWithRandomNumber();
    }
}