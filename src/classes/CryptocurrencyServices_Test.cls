@IsTest
private class CryptocurrencyServices_Test {

    public static void testSetupMock() {
        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);
    }

    @IsTest
    static void test_getCryptoNames() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response.setStatusCode(200);
        mock.addResponse(response);

        Test.setMock(HttpCalloutMock.class, mock);
        String result = CryptocurrencyServices.getCryptoNames('BTC');
        System.assertEquals('Bitcoin', result);
    }

    @IsTest
    static void test_getCryptoNamesNegative() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('');
        response.setStatusCode(404);
        mock.addResponse(response);

        Test.setMock(HttpCalloutMock.class, mock);
        String result = CryptocurrencyServices.getCryptoNames('BTC');
        System.assertEquals(null, result);
    }

    @IsTest
    static void test_getCryptoDetails() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        Test.setMock(HttpCalloutMock.class, mock);
        Decimal result = CryptocurrencyServices.getCryptoDetails('BTC');
        System.assertEquals(57921.01, result);
    }

    @IsTest
    static void test_getIconUrl() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response.setStatusCode(200);
        mock.addResponse(response);

        Test.setMock(HttpCalloutMock.class, mock);
        String result = CryptocurrencyServices.getIconURL('BTC');
        System.assertEquals('https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png', result);
    }

    @IsTest
    static void test_getDataCryptocurrencies() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);

        Test.setMock(HttpCalloutMock.class, mock);
        Cryptocurrency__c cryptocurrency = new Cryptocurrency__c();
        cryptocurrency.Name = 'BTC';
        cryptocurrency.Short_Description__c = 'Test';
        insert cryptocurrency;
        Test.startTest();
        List<Cryptocurrency__c> cryptocurrencies = CryptocurrencyServices.getDataCryptocurrencies('Name', 'DESC');
        Test.stopTest();
        System.assertEquals(1, cryptocurrencies.size(), 'Expected to get 1');
    }

    @IsTest
    static void test_downloadPDFData() {
        String fileData = 'I am String to be converted in base64 encoding!';
        String fileDateBase64 = EncodingUtil.base64Encode(Blob.valueOf(fileData));
        String result = CryptocurrencyServices.DownloadPDFData();
        System.assertEquals(fileDateBase64, result);
    }

    @IsTest
    static void test_getTotalAmountOfCryptocurrencies() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);

        Test.setMock(HttpCalloutMock.class, mock);
        Cryptocurrency__c cryptocurrency = new Cryptocurrency__c();
        cryptocurrency.Name = 'BTC';
        cryptocurrency.Short_Description__c = 'Test';
        insert cryptocurrency;

        Account account = new Account(Name = 'Test Account');
        insert account;

        Cryptocurrency_Account__c cryptocurrencyAccount = new Cryptocurrency_Account__c(Cryptocurrency__c = cryptocurrency.Id, Account__c = account.Id);
        insert cryptocurrencyAccount;

        List<Account> results = [SELECT Id, Name, Total_Cryptocurrencies__c FROM Account];
        for (Account acc : results) {
            if (acc.Total_Cryptocurrencies__c == 1.0) {
                System.assertEquals(1, acc.Total_Cryptocurrencies__c);
            }
        }
    }

    @IsTest
    static void test_saveCryptocurrency() {
        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);

        Test.setMock(HttpCalloutMock.class, mock);
        Cryptocurrency__c cryptocurrency = new Cryptocurrency__c();
        cryptocurrency.Name = 'BTC';
        cryptocurrency.Short_Description__c = 'Test';
        CryptocurrencyServices.saveCryptocurrency(cryptocurrency);
        List<Cryptocurrency__c> cryptocurrencies = [SELECT Id, Name FROM Cryptocurrency__c];

        System.assertEquals(1, cryptocurrencies.size());
    }

}