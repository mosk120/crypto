@IsTest
private class CryptocurrencyTablePDFController_Test {

    @IsTest
    static void test_getCurrencies() {

        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);
        Test.setMock(HttpCalloutMock.class, mock);
        Cryptocurrency__c cryptocurrency = new Cryptocurrency__c(Name = 'BTC', Short_Description__c = 'Test Description');
        insert cryptocurrency;

        CryptocurrencyTablePDFController controller = new CryptocurrencyTablePDFController();
        List<Cryptocurrency__c> cryptocurrencies = controller.getCurrencies();
        System.assertEquals(1, cryptocurrencies.size(), 'Expected to get 1');
    }
}