public with sharing class GeneralHttpMock implements HttpCalloutMock {
    HttpResponse[] responses = new HttpResponse[0];
    public void addResponse(HttpResponse response) {
        responses.add(response);
    }

    public HttpResponse respond(HttpRequest request) {
        return responses.remove(0);
    }

    /* Example of usage in test
    GeneralHttpMock mock = new GeneralHttpMock();
    HttpResponse response = new HttpResponse();
    response.setHeader('Content-Type', 'application/json');

    response.setBody('test body');
    response.setStatusCode(201);
    mock.addResponse(response);
    Test.setMock(HttpCalloutMock.class, mock);
    */

}
