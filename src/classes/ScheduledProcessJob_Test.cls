@IsTest
private class ScheduledProcessJob_Test {
    @IsTest
    static void test_startScheduler()  {
        Test.startTest();
        ScheduledProcessJob.startScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        Boolean result = ScheduledProcessJob.isSchedulerRunning(CryptocurrencySchedulable.SCHEDULED_JOB);
        Test.stopTest();
        List<CronTrigger> jobs = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = :CryptocurrencySchedulable.SCHEDULED_JOB
        ];
        System.assertEquals(1, jobs.size());
        System.assertEquals(true, result);
    }
    @IsTest
    static void test_doubleScheduler() {
        Test.startTest();
        ScheduledProcessJob.startScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        ScheduledProcessJob.startScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        Test.stopTest();
        List<CronTrigger> jobs = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = :CryptocurrencySchedulable.SCHEDULED_JOB
        ];
        System.assertEquals(1, jobs.size());
    }

    @IsTest
    static void test_stopScheduler() {
        Test.startTest();
        ScheduledProcessJob.startScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        ScheduledProcessJob.stopScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        Boolean result = ScheduledProcessJob.isSchedulerRunning(CryptocurrencySchedulable.SCHEDULED_JOB);
        Test.stopTest();
        List<CronTrigger> jobs = [
                SELECT Id
                FROM CronTrigger
                WHERE CronJobDetail.Name = :CryptocurrencySchedulable.SCHEDULED_JOB
        ];
        System.assertEquals(0, jobs.size());
        System.assertEquals(false, result);
    }

    @IsTest
    static void test_stopSchedulerException() {
        Test.startTest();
        ScheduledProcessJob.stopScheduler(null);
        Boolean result = ScheduledProcessJob.stopScheduler(null);
        Test.stopTest();
        System.assertEquals(result, true);
    }

    @IsTest
    static void test_isSchedulerRunning() {
        Test.startTest();
        ScheduledProcessJob.startScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        Boolean result = ScheduledProcessJob.isSchedulerRunning(CryptocurrencySchedulable.SCHEDULED_JOB);
        Test.stopTest();
        System.assertEquals(true, result);
    }

    @IsTest
    static void test_isSchedulerRunningNegative() {
        Test.startTest();
        ScheduledProcessJob.stopScheduler(CryptocurrencySchedulable.SCHEDULED_JOB);
        Boolean result = ScheduledProcessJob.isSchedulerRunning(CryptocurrencySchedulable.SCHEDULED_JOB);
        Test.stopTest();
        System.assertEquals(false, result);
    }
}