public class Utils {
    public static void fieldIsEmptyError(List<SObject> records, String fieldName, String message) {
        for (SObject record : (List<SObject>) records) {
            if (record.get(fieldName) == null) {
                record.addError(fieldName, message);
            }
        }
    }

    public static void getValuesByFieldName(Set<Id> AccountIds, List<SObject> records, String fieldName) {
        for (SObject record : records) {
            String value = (String) record.get(fieldName);
            AccountIds.add(value);
            if (value != null) {
                AccountIds.add(value);
            }
        }
    }

//    public static Set<String> getSetOfStringValuesFromList(List<SObject> records, String fieldName) {
//        Set<String> results = new Set<String>();
//
//        for (SObject record : records) {
//            String value = (String) record.get(fieldName);
//            if (value != null) {
//                results.add(value);
//            }
//        }
//        return results;
//    }
//
//    // Method can be called from before/after update. Returns list of objects with specified field changed.
//    public static List<SObject> getModifiedRecords(List<String> fields, List<SObject> records, Map<Id, SObject> oldRecords) {
//        List<SObject> results = new List<SObject>();
//        //check if new records aren't empty
//        if (records != null) {
//            for (SObject record : records) {
//                SObject newRecord = record;
//                String recordId = record.Id;
//                SObject oldRecord = (oldRecords != null) ? oldRecords.get(recordId) : null;
//                //check if old record equals null
//                if (oldRecord == null) {
//                    for (String field : fields) {
//                        //check if new field isn't empty
//                        if (newRecord.get(field) != null) {
//                            //add to results if it's not empty
//                            results.add(newRecord);
//                            break;
//                        }
//                    }
//                } else {
//                    for (String field : fields) {
//                        //Check if new field isn't equals old field
//                        if (oldRecord.get(field) != newRecord.get(field)) {
//                            results.add(newRecord);
//                            break;
//                        }
//                    }
//                }
//            }
//            //if old records aren't empty add all record values to the list results
//        } else if (oldRecords != null) {
//            results.addAll(oldRecords.values());
//        }
//        return results;
//    }
//
//    public static List<Map<String, String>> getPicklistEntries(Schema.SObjectType sObjectType, String fieldName) {
//        String apiName = sObjectType.getDescribe().getName();
//
//        DescribeSObjectResult objResult = Schema.getGlobalDescribe()
//                .get(apiName).getDescribe();
//        DescribeFieldResult fieldResult = objResult.fields.getMap()
//                .get(fieldName).getDescribe();
//        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
//        List<Map<String, String>> resultList = new List<Map<String, String>>();
//        for (Schema.PicklistEntry item : picklistEntries) {
//            resultList.add(new Map<String, String>{
//                    item.getLabel() => item.getValue()
//            });
//        }
//        System.debug(resultList);
//        for (Map<String, String> key : resultList) {
//            System.debug(key);
//        }
//        return resultList;
//    }
//
//    public static String findObjectNameFromRecordIdPrefix(String recordIdOrPrefix) {
//        String prefix = recordIdOrPrefix.substring(0, 3);
//        String apiName = '';
//        System.debug(Schema.getGlobalDescribe().values());
//        for (Schema.SObjectType obj : Schema.getGlobalDescribe().values()) {
//            String prefixToCompare = obj.getDescribe().getKeyPrefix();
//            if (prefix == prefixToCompare) {
//                apiName = obj.getDescribe().getName();
//            }
//        }
//        return apiName;
//    }
//
//    public static Map<SObject, Object> getParentIdToChildrenMapping(List<SObject> childrenRecords, String fieldName) {
//        Map<SObject, Object > results = new Map<SObject, Object>();
//
//        for (SObject record : childrenRecords) {
//            SObject newRecord = record;
//            results.put(newRecord, record.get(fieldName));
//        }
//        return results;
//    }
}