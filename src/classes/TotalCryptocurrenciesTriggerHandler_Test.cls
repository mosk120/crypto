@IsTest
private class TotalCryptocurrenciesTriggerHandler_Test {
    @IsTest
    static void test_TotalCryptocurrencies() {
        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin", "rate": 57220.370564244571124717816721}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);

        Test.setMock(HttpCalloutMock.class, mock);
        Account account = new Account(Name='Test Account');
        insert account;
        Cryptocurrency__c cryptocurrency = new Cryptocurrency__c(Account__c = account.Id, Name = 'BTC', Short_Description__c = 'Test Description');
        insert cryptocurrency;

        Cryptocurrency_Account__c cryptocurrencyAccount = new Cryptocurrency_Account__c(Cryptocurrency__c = cryptocurrency.Id, Account__c = account.Id);
        insert cryptocurrencyAccount;

        List<Account> results = [SELECT Id, Name, Total_Cryptocurrencies__c FROM Account];
        for (Account acc : results) {
                System.assertEquals(1, acc.Total_Cryptocurrencies__c);
        }
    }

    @IsTest
    static void test_TotalCryptocurrenciesNegative() {
        GeneralHttpMock mock = new GeneralHttpMock();
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rate": 57921.007898114888371576094388}');
        response.setStatusCode(200);
        mock.addResponse(response);

        HttpResponse response2 = new HttpResponse();
        response2.setHeader('Content-Type', 'application/json');
        response2.setBody('[{"name":"Bitcoin"}, {"name": "Ripple"}]');
        response2.setStatusCode(200);
        mock.addResponse(response2);

        HttpResponse response3 = new HttpResponse();
        response3.setHeader('Content-Type', 'application/json');
        response3.setBody('[\n' +
                '{\n' +
                '        "asset_id": "BTC",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"\n' +
                '    }\n' +
                ']');
        response3.setStatusCode(200);
        mock.addResponse(response3);
        HttpResponse response4 = new HttpResponse();
        response4.setHeader('Content-Type', 'application/json');
        response4.setBody('{"rate": 1.4342949215232908822237968119}');
        response4.setStatusCode(200);
        mock.addResponse(response4);

        HttpResponse response5 = new HttpResponse();
        response5.setHeader('Content-Type', 'application/json');
        response5.setBody('[{"name": "Ripple"}]');
        response5.setStatusCode(200);
        mock.addResponse(response5);

        HttpResponse response6 = new HttpResponse();
        response6.setHeader('Content-Type', 'application/json');
        response6.setBody('[\n' +
                '{\n' +
                '        "asset_id": "XRP",\n' +
                '        "url": "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/ba90bcb0cafb4801ac5dd310f47d6411.png"\n' +
                '    }\n' +
                ']');
        response6.setStatusCode(200);
        mock.addResponse(response6);

        Test.setMock(HttpCalloutMock.class, mock);
        Account account = new Account(Name='Test Account');
        insert account;
        List<Cryptocurrency__c> cryptocurrencies = new List<Cryptocurrency__c>();
        Cryptocurrency__c cryptocurrency = new Cryptocurrency__c(Account__c = account.Id, Name = 'BTC', Short_Description__c = 'Test Description');
        insert cryptocurrency;
//        cryptocurrencies.add(cryptocurrency);
        Cryptocurrency__c cryptocurrency2 = new Cryptocurrency__c(Account__c = account.Id, Name = 'XRP', Short_Description__c = 'Test Description');
        insert cryptocurrency2;
//        cryptocurrencies.add(cryptocurrency2);
//        insert cryptocurrencies;

        Cryptocurrency_Account__c cryptocurrencyAccount = new Cryptocurrency_Account__c(Cryptocurrency__c = cryptocurrency.Id, Account__c = account.Id);
        insert cryptocurrencyAccount;

        Cryptocurrency_Account__c cryptocurrencyAccount2 = new Cryptocurrency_Account__c(Cryptocurrency__c = cryptocurrency2.Id, Account__c = account.Id);
        insert cryptocurrencyAccount2;
        delete cryptocurrencyAccount2;

        List<Account> results = [SELECT Id, Name, Total_Cryptocurrencies__c FROM Account];
        for (Account acc : results) {
            System.assertEquals(1, acc.Total_Cryptocurrencies__c);
        }

//        Integer result = [SELECT COUNT() FROM Cryptocurrency__c];
//        System.assertEquals(2, result);
    }
}