public class CryptocurrencySchedulable implements Schedulable {
    public static final String SCHEDULED_JOB = 'ScheduledProcessJob';

    public interface IScheduleDispatched {
        void executeTask(SchedulableContext sc);
    }

    public void execute(SchedulableContext sc) {
        Type targetType = Type.forName(CryptocurrencySchedulable.SCHEDULED_JOB);
        if (targetType != null) {
            IScheduleDispatched instance = (IScheduleDispatched) targetType.newInstance();
            instance.executeTask(sc);
        }
    }
}
