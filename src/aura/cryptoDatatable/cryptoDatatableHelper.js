({
    getInitialData: function (component, event, helper) {
        this.showSpinner(component);
        var action = component.get('c.getDataCryptocurrencies');
        action.setParams({
            'field': component.get('v.sortBy'),
            'sortOrder': component.get('v.sortOrder')
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.cryptocurrencies', response.getReturnValue());
                this.hideSpinner(component);
            } else {
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    createCryptocurrency: function (component) {
        var action = component.get('c.saveCryptocurrency');
        action.setParams({
            'cryptocurrency': component.get('v.newCryptocurrency')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                this.showToast(component);
                this.clearFields(component);
                this.getInitialData(component);
            } else if (state === 'ERROR') {
                console.log('Problem saving cryptocurrency, response state: ' + state);
            } else {
                console.log('Unknown problem, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    sortHelper: function (component, event, helper) {
        var sortBy = event.target.id;

        var previousSortBy = component.get('v.sortBy');
        var previousSortDirection = component.get('v.sortOrder');
        component.set('v.sortBy', sortBy);

        if (previousSortBy === sortBy && previousSortDirection === 'DESC') {
            component.set('v.sortOrder', 'ASC');
        } else if (previousSortBy === sortBy && previousSortDirection === 'ASC') {
            component.set('v.sortOrder', 'DESC');
        } else if (previousSortBy !== sortBy) {
            component.set('v.sortOrder', 'ASC');
        }
        this.getInitialData(component, event, helper);
    },

    showSpinner: function (component) {
        component.set('v.IsSpinner', true);
    },

    hideSpinner: function (component) {
        component.set('v.IsSpinner', false);
    },

    downloadPDF: function (component, event, helper) {
        helper.showSpinner(component);
        var action = component.get('c.DownloadPDFData');

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                helper.hideSpinner(component);
                var downloadLink = document.createElement('a');
                downloadLink.setAttribute('type', 'hidden');
                downloadLink.href = 'data:application/pdf;base64,' + response.getReturnValue();
                downloadLink.download = 'CryptocurrencyExchangeRatesReport.pdf';
                document.body.appendChild(downloadLink);
                downloadLink.click();
                downloadLink.remove();
            } else {
                console.log('Failed with state: ' + state);
            }
        })

        $A.enqueueAction(action);
    },

    showToast: function (component, event, helper) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: 'Success',
            message: 'Cryptocurrency has been created successfully.',
            type: 'success',
        });
        toastEvent.fire();
    },

    clearFields: function (component, event, helper) {
        component.set('v.newCryptocurrency', {
            'sobjectType': 'Cryptocurrency__c',
            'Name': '',
            'Price_In_USD__c': '',
            'Abbrev__c': '',
            'Short_Description__c': ''
        })
    },

});