({
    doInit: function (component, event, helper) {
        helper.getInitialData(component, event, helper);
    },

    sortHandler: function (component, event, helper) {
        helper.sortHelper(component, event, helper);
    },

    downloadPDFHandler: function (component, event, helper) {
        helper.downloadPDF(component, event, helper);
    },

    showModal: function (component) {
        component.set('v.IsModal', true);
    },

    hideModal: function (component) {
        component.set('v.IsModal', false);
    },

    createCryptocurrencyHandler: function (component, event, helper) {
        helper.createCryptocurrency(component, event, helper);
    },

    clearFieldsHandler: function (component, event, helper) {
        helper.clearFields(component, event, helper);
    },

    navigateToRecord: function (component, event, helper) {
        var index = event.target.getAttribute('data-index');
        var cryptocurrency = component.get('v.cryptocurrencies')[index];
        var navEvent = $A.get("e.force:navigateToSObject");
        navEvent.setParams({
            recordId: cryptocurrency.Id,
            slideDevName: "detail"
        });
        navEvent.fire();
    }
});