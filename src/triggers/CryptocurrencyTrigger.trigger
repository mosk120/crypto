trigger CryptocurrencyTrigger on Cryptocurrency__c (before insert, after insert) {
    new CryptocurrencyTriggerHandler().run();
}