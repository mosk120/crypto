trigger TotalCryptocurrencyTrigger on Cryptocurrency_Account__c (before insert, after insert, after delete) {
    new TotalCryptocurrenciesTriggerHandler().run();
}